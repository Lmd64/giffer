//
//  DBBGifViewController.h
//  Giffer
//
//  Created by Liam Dunne on 11/10/2013.
//  Copyright (c) 2013 iRare Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DropboxSDK/DropboxSDK.h>

@class DBRestClient;
@class DBMetadata;

@protocol DBBGifViewerDelegate <NSObject>
- (void)backButtonTapped:(id)sender;
@end

@interface DBBGifViewController : UIViewController <UIScrollViewDelegate,UITextFieldDelegate> {
    DBRestClient *restClient;
}
@property (nonatomic,assign) id <DBBGifViewerDelegate> delegate;
@property (nonatomic,strong) NSString *path;
@property (nonatomic,strong) DBMetadata *file;
@end
