//
//  DBBGifViewController.m
//  Giffer
//
//  Created by Liam Dunne on 11/10/2013.
//  Copyright (c) 2013 iRare Media. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "DBBGifViewController.h"
#import "GIFLoader.h"

@interface DBBGifViewController () <DBRestClientDelegate> {
    DBMetadata *selectedFile;
}
@property (nonatomic,strong) UIScrollView *scrollview;
@property (nonatomic,strong) UITextField *textField;
@property (nonatomic,strong) UIImageView *imageView;
@end

@implementation DBBGifViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:[UIColor lightGrayColor]];

    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(backButtonTapped:)];
    [swipe setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.view addGestureRecognizer:swipe];

    self.scrollview = [[UIScrollView alloc] initWithFrame:self.view.frame];
    [self.scrollview setDelegate:self];
    [self.scrollview setMinimumZoomScale:1.0];
    [self.scrollview setMaximumZoomScale:4.0];
    [self.view addSubview:self.scrollview];

    self.imageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [self.imageView setContentMode:UIViewContentModeCenter];
    [self.imageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.scrollview addSubview:self.imageView];

    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [backButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [backButton setTitle:@"back" forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    [backButton sizeToFit];

    self.textField = [[UITextField alloc] initWithFrame:self.view.frame];
    [self.textField setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSString *oldFileName = [[[self.path pathComponents] lastObject] stringByDeletingPathExtension];
    [self.textField setText:oldFileName];
    [self.textField setBorderStyle:UITextBorderStyleBezel];
    [self.textField setReturnKeyType:UIReturnKeyDone];
    [self.textField setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.75]];
    [self.textField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [self.textField setPlaceholder:NSLocalizedString(@"enter filename", nil)];
    [self.textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [self.textField setDelegate:self];
    [self.view addSubview:self.textField];
    
    CGSize backButtonSize = [backButton sizeThatFits:backButton.frame.size];
    
    NSDictionary *views = @{@"backButton":backButton,
                            @"textField":self.textField,
                            @"topLayoutGuide":self.topLayoutGuide};
    NSDictionary *metrics = @{@"backButtonSize":@(backButtonSize.width)};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[backButton(backButtonSize)]-[textField]-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[topLayoutGuide]-[backButton]" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[topLayoutGuide]-[textField(32)]" options:0 metrics:metrics views:views]];

    [backButton setContentCompressionResistancePriority:UILayoutPriorityFittingSizeLevel forAxis:UILayoutConstraintAxisHorizontal|UILayoutConstraintAxisVertical];
    [backButton setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal|UILayoutConstraintAxisVertical];
    [self.textField setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal|UILayoutConstraintAxisVertical];
    [self.textField setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal|UILayoutConstraintAxisVertical];
    
    NSAssert(self.path, @"self.path not set!");
    NSData* data = [[NSData alloc] initWithContentsOfFile:self.path];
    [GIFLoader loadGIFData:data to:self.imageView];

    [self.imageView sizeToFit];
    [self.imageView setCenter:self.view.center];
    
    [self.imageView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.imageView.layer setShadowOffset:CGSizeMake(0, 1)];
    [self.imageView.layer setShadowOpacity:0.5];
    [self.imageView.layer setShadowPath:[UIBezierPath bezierPathWithRect:self.imageView.bounds].CGPath];
    [self.imageView.layer setShadowRadius:2.0];
    
    
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
    UIView *subView = [scrollView.subviews objectAtIndex:0];
    
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.imageView;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSMutableCharacterSet *allowedCharacters = [NSCharacterSet alphanumericCharacterSet];
    NSCharacterSet *invalidCharacters = [allowedCharacters invertedSet];
    if ([string isEqualToString:@"-"] || [string isEqualToString:@"_"]){
        //we'll allow it
    } else if ([string isEqualToString:@" "]){
        //we'll allow it
        string = @"_";
    } else if ([string rangeOfCharacterFromSet:invalidCharacters].location != NSNotFound){
        return NO;
    }
    
    if([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    NSUInteger newLength = [textField.text length] - range.length + [string length];
    if (newLength >= 32) {
        textField.text = [[textField.text stringByReplacingCharactersInRange:range withString:string] substringToIndex:32];
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)backButtonTapped:(id)sender{
    [self renameFile];
    if ([self.delegate respondsToSelector:@selector(backButtonTapped:)]){
        [self.delegate performSelector:@selector(backButtonTapped:) withObject:sender];
    }
}

- (void)renameFile{
    NSString *extension = [self.path pathExtension];
    NSString *oldFileName = [[[self.path pathComponents] lastObject] stringByDeletingPathExtension];
    NSString *newFileName = self.textField.text;
    if (newFileName.length==0){
        [self.textField setText:oldFileName];
        return;
    }
    newFileName = [newFileName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    newFileName = [newFileName stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    if (![oldFileName isEqualToString:newFileName]){
        NSString *rootPath;
        NSString *fromPath;
        NSString *toPath;
        
        //    rename local file
        rootPath = [self.path stringByDeletingLastPathComponent];
        fromPath = [[rootPath stringByAppendingPathComponent:oldFileName] stringByAppendingPathExtension:extension];
        toPath = [[rootPath stringByAppendingPathComponent:newFileName] stringByAppendingPathExtension:extension];
        NSLog(@"LOCAL");
        NSLog(@"from: %@",fromPath);
        NSLog(@"to:   %@",toPath);
        NSError *error;
        if ([[NSFileManager defaultManager] moveItemAtPath:fromPath toPath:toPath error:&error]){
        } else{
            NSLog(@"couldn't rename\n%@ to\n%@",fromPath,toPath);
        }
        
        //    rename remote file
        rootPath = [[self.file path] stringByDeletingLastPathComponent];
        fromPath = [[rootPath stringByAppendingPathComponent:oldFileName] stringByAppendingPathExtension:extension];
        toPath = [[rootPath stringByAppendingPathComponent:newFileName] stringByAppendingPathExtension:extension];
        NSLog(@"REMOTE");
        NSLog(@"from: %@",fromPath);
        NSLog(@"to:   %@",toPath);
        
        [[self restClient] moveFrom:(NSString*)fromPath toPath:(NSString *)toPath];

    }
}

- (DBRestClient *)restClient {
    if (!restClient) {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

- (void)rightButtonTapped:(id)sender{
    //copy URL?
    //share image?
    //what to do, what to do...
}

@end
